aptf
====

APT-F is a frontend for apt-get, aptitude, add-apt-repository, and pip that
formats requests for you. It separates commands from packages syntactically,
so you can install and remove packages (or perform any other apt-get task)
by invoking it only once. APT-F also automatically calls sudo for all
commands, and tries apt repositories first before performing pip
system-level installs for python packages not in the apt repositories.

Options are passed to programs.
Options specified before a command will be passed to all commands and programs.
Options specified after the first command will be passed to the previous command
and program only.

APT-F has some options of its own; these are not passed:
-a			ask the user before applying changes
-n			no-halt; continue even if one command encounters errors
--aptitude	prefer aptitude to apt-get in cases where their commands overlap
--dump		if there are errors, dump stdout in addition to stderr
--echo		echo the generated commands before executing
--echo-only	echo and do not execute (no-act)
--silent	do not show anything
--prog		show a progress bar only

Unlike apt-get, APT-F opts to ask the user only if requested to, to allow easy
batch operations. All commands generated with aptf will be given the -y command
unless you pass -a. -a is not passed to programs. For pip, -a is equivalent
to --dry-run.

APT-F supports all commands supported by apt-get (preferred) and aptitude (when
not handled by apt-get). It also supports:
add-rep          - calls add-apt-repository (Ubuntu script)
rem-rep          - shorthand for add-rep --remove
autopurge        - shorthand for autoremove --purge
complete-upgrade - shorthand for autoremove update dist-upgrade autoremove
                   && pip install -U

Using APT-F, a standard apt-get procedure such as:

$ sudo apt-get -y remove firefox && sudo apt-get -y update && sudo apt-get -y dist-upgrade && sudo apt-get -y autoremove && sudo apt-get -y install midori

becomes:

$ aptf remove firefox update dist-upgrade autoremove install midori
