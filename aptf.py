#!/usr/bin/python3

import sys,subprocess,time,queue,threading

def show_help():
	if "apt-get" in sys.argv:
		subprocess.call("apt-get help")
	elif "aptitude" in sys.argv:
		subprocess.call("aptitude help")
	elif "add-rep" in sys.argv:
		subprocess.call("add-apt-repository -h")
	else:
		print("""
APT-F frontend for debian apt-get, aptitude, and ubuntu add-apt-repository
1.0.2
Usage:	aptf [globaloptions]
			[command1] [options] [packages]
			[command2] [options] ...

APT-F is a frontend for apt-get, aptitude, add-apt-repository, and pip that
formats requests for you. It separates commands from packages syntactically,
so you can install and remove packages (or perform any other apt-get task)
by invoking it only once. APT-F also automatically calls sudo for all
commands, and tries apt repositories first before performing pip
system-level installs for python packages not in the apt repositories.

Options are passed to programs.
Options specified before a command will be passed to all commands and programs.
Options specified after the first command will be passed to the previous command
and program only.

APT-F has some options of its own; these are not passed:
-a			ask the user before applying changes
-n			no-halt; continue even if one command encounters errors
--aptitude	prefer aptitude to apt-get in cases where their commands overlap
--dump		if there are errors, dump stdout in addition to stderr
--echo		echo the generated commands before executing
--echo-only	echo and do not execute (no-act)
--silent	do not show anything
--prog		show a progress bar only

Unlike apt-get, APT-F opts to ask the user only if requested to, to allow easy
batch operations. All commands generated with aptf will be given the -y command
unless you pass -a. -a is not passed to programs.

APT-F supports all commands supported by apt-get (preferred) and aptitude (when
not handled by apt-get). It also supports:
add-rep          - calls add-apt-repository (Ubuntu script)
rem-rep          - shorthand for add-rep --remove
autopurge        - shorthand for autoremove --purge
complete-upgrade - shorthand for autoremove update dist-upgrade autoremove
                   && pip install -U

To see the help message from one of the programs APT-F calls, call
help/-h/--help with the arguments "apt-get", "aptitude", or "add-rep".
""")
	sys.exit(0)

def generate_commands(command,globaloptions,options,packages):
	if command=="complete-upgrade":
		return generate_single_command("autoremove",globaloptions,options,[]),generate_single_command("update",globaloptions,options,[]),generate_single_command("dist-upgrade",globaloptions,options,packages),generate_single_command("autoremove",globaloptions,options,[]),("sudo pip3 --disable-pip-version-check list --outdated --format=json | python3 -c \"import json, sys; print('\\n'.join([x['name'] for x in json.load(sys.stdin)]))\" | sudo xargs -n1 pip3 --disable-pip-version-check install --break-system-packages -U",[x for x in globaloptions if x in aptfoptions]+[x for x in options if x in aptfoptions]+["--noerror"])
	else:
		python_packages=[]
		other_packages=[]
		for x in packages:
			if x.startswith("python3-"):
				python_packages.append(x)
			else:
				other_packages.append(x)
		cmds=[]
		if len(python_packages)>0:
			for p in python_packages:
				cmds.append(generate_single_command(command,globaloptions,options,(p,),True))
		cmds.append(generate_single_command(command,globaloptions,options,other_packages))
		return tuple(cmds)

def generate_single_command(command,globaloptions,options,packages,python=False):
	st="sudo "

	program="aptitude"
	if command in aptgetcommands and "--aptitude" not in options and "--aptitude" not in globaloptions or command not in aptitudecommands:
		program="apt-get"
	if command in addrepcommands:
		program="add-apt-repository"

	st+=program+" "

	if command == "autopurge":
		command="autoremove --purge"
	if command == "add-rep":
		command=""
	if command == "rem-rep":
		command="--remove"

	st+=command+" "

	if "-a" not in options and "-a" not in globaloptions:
		st+="-y "
	for x in globaloptions:
		if x not in aptfoptions:
			st+=x+" "
	for x in options:
		if x not in aptfoptions:
			st+=x+" "

	for x in packages:
		st+=x+" "

	if python and command in pipcommands:
		st+="|| sudo pip3 --disable-pip-version-check "

		if command=="remove":
			command="uninstall"

		st+=command+" --break-system-packages "

		if "-a" in options or "-a" in globaloptions and command!="uninstall":
			st+="--dry-run "
		if "-a" not in options and "-a" not in globaloptions and command=="uninstall":
			st+="-y "
		for x in globaloptions:
			if x not in aptfoptions:
				st+=x+" "
		for x in options:
			if x not in aptfoptions:
				st+=x+" "

		for x in packages:
			st+=x.replace("python3-","")+" "

	instr=[x for x in globaloptions if x in aptfoptions]+[x for x in options if x in aptfoptions]

	return (st,instr)

def nonblocking_read(st,qu):
	for line in iter(st.readline, b''):
		qu.put(line)
	st.close()

def process_args():
	global optionlist,packagelist
	currentcommand=""
	for arg in sys.argv[1:]:
		if arg[0]=="-":
			if arg[1]=="-":
				if arg=="--help":
					show_help()
				elif currentcommand=="":
					globallist.append(arg)
				else:
					optionlist.append(arg)
			else:
				if arg=="-h":
					show_help()
				elif currentcommand=="":
					globallist.append(arg)
				else:
					optionlist.append(arg)
		else:
			if arg=="help":
				show_help()
			if arg in aptgetcommands or arg in aptitudecommands or arg in addrepcommands:  # pip commands are a strict subset of the union of aptget and aptitude
				currentoptions=optionlist
				optionlist=[]
				if currentcommand!="":
					currentpackages=packagelist
					packagelist=[]
					c=generate_commands(currentcommand,globallist,currentoptions,currentpackages)
					for x in c:
						batchlist.append(x)
				currentcommand=arg
			else:
				packagelist.append(arg)
	if currentcommand!="":
		c=generate_commands(currentcommand,globallist,optionlist,packagelist)
		for x in c:
			batchlist.append(x)
	else:
		if len(packagelist)==0:
			show_help()
		else:
			print("No valid command specified: "+packagelist[0])
			sys.exit(1)

def execute_single_command(command_num,command):
	global notfinished,quietlen
	err=""
	out=""
	if "--echo" in command[1] or "--echo-only" in command[1]:
		suffix="&&"
		if "-n" in command[1]:
			suffix=";"
		print(
			"""
				
				== APT-F =======================================================================
				"""+command[0]+suffix+"""
	================================================================================"""
		)
	if "--echo-only" not in command[1]:
		if "--prog" in command[1] and "--silent" not in command[1]:
			if "--echo" not in command[1]:
				for g in range(0,quietlen):
					print("\b",end="")

			stri="["
			for g in range(0,command_num):
				stri+="#"
			for g in range(command_num,len(batchlist)):
				stri+=" "
			stri+="] "+str(int(command_num/(len(batchlist))*100))+"% "
			quietlen=len(stri)
			print(stri,end="")
			sys.stdout.flush()

		process=subprocess.Popen(command[0]+("-q " if "--prog" in command[1] or "--silent" in command[1] else ""),stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
		stde=queue.Queue()
		stdou=queue.Queue()
		t1=threading.Thread(target=nonblocking_read,args=(process.stdout,stdou))
		t2=threading.Thread(target=nonblocking_read,args=(process.stderr,stde))
		t1.daemon=True
		t2.daemon=True
		t1.start()
		t2.start()
		if "--prog" in command[1]:
			spinny=('|','/','-','\\')
			spinnycounter=0
			while process.poll() is None:
				try:
					if "--silent" not in command[1]:
						print(spinny[spinnycounter%4],end="")
						sys.stdout.flush()
					time.sleep(0.2)
					if "--silent" not in command[1]:
						spinnycounter+=1
						spinnycounter%=4
						print("\b",end="")
				except KeyboardInterrupt:
					process.kill()
					sys.exit()
			sys.stdout.flush()
			while not stde.empty():
				err+=str(stde.get_nowait(),"utf-8")
			while not stdou.empty():
				out+=str(stdou.get_nowait(),"utf-8")
		else:
			while process.poll() is None:
				try:
					while not stde.empty():
						err+=str(stde.get_nowait(),"utf-8")
					while not stdou.empty():
						o=str(stdou.get_nowait(),"utf-8")
						out+=o
						print(o,end="")
				except KeyboardInterrupt:
					process.kill()
					sys.exit()
		sys.stdout.flush()
		retval=process.returncode

		if "--noerror" in command[1]:
			retval=0
			err=""

		results.append((retval,err,out))
		if len(err)!=0:
			if "-n" not in command[1]:
				print("\n")
				notfinished=True
				return False
	return True

def process_results():
	global quietlen
	successes=0
	failures=0
	for x in results:
		if x[0]==0:
			successes+=1
		else:
			failures+=1
	notex=len(batchlist)-len(results)
	if "--prog" in globallist and "--silent" not in globallist and not notfinished:
		for g in range(0,quietlen):
			print("\b",end="")
		stri="["
		for g in range(0,len(batchlist)):
			stri+="#"
		stri+="] "+str(100)+"% "
		quietlen=len(stri)
		print(stri)
		sys.stdout.flush()
	if failures!=0:
		print("\n"+str(successes)+" succeeded, "+str(failures)+" failed, "+str(notex)+" not executed\n")
		print("Failing operations:")
		for x in range(0,len(results)):
			if results[x][0]!=0:
				print(batchlist[x][0])
				print(
					"\nError "+str(results[x][0])+":"+"""
	================================================================================"""
				)
				if "--dump" in batchlist[x][1]:
					print(results[x][2]+"""================================================================================""")
				print(
					results[x][1]+"""================================================================================
	"""
				)

if __name__ == "__main__":
	aptgetcommands=("update","upgrade","install","remove","autoremove","purge","source","build-dep","dist-upgrade","dselect-upgrade","clean","autoclean","check","changelog","download","autopurge","complete-upgrade")
	aptitudecommands=("install","remove","purge","hold","unhold","markauto","unmarkauto","forbid-version","update","safe-upgrade","full-upgrade","build-dep","forget-new","search","show","versions","clean","autoclean","changelog","download","reinstall","why","why-not")
	addrepcommands=("add-rep","rem-rep")
	pipcommands = ("install","download","remove","check","search")

	aptfoptions=("-a", "-n", "--aptitude","--echo","--echo-only","--prog","--silent","--dump")

	subprocess.call("sudo pwd > /dev/null",shell=True)
	globallist=[]
	optionlist=[]
	packagelist=[]
	batchlist=[]

	process_args()

	i=-1
	quietlen=0
	notfinished=False
	results=[]

	for x in batchlist:
		i+=1
		if not execute_single_command(i,x):
			break

	process_results()

	print("\n",end="")
	sys.stdout.flush()
