#!/bin/bash
sudo cp aptf.py /usr/local/bin/aptf
sudo chmod 755 /usr/local/bin/aptf
echo "APT-F attempts to call apt-get, aptitude, add-apt-repository, and pip3 for"
echo "various commands. Install those programs as needed/desired."
